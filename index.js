console.log("Bamboozled!");
//JS - is a loosely type programming language

/*alert("Hello Again!");*/

console.log("Hello World!");

console.
log
(
    "Hello World!"
);

/* Multi line comment*/

// [SECTION] - Variables
// used to contain data
// usually stored in a computer's memory

// Declaration of variable

// Syntax --> let variableName
let myVariable;

console.log(myVariable);

// Syntax --> let variableName variableValue
let mySecondVariable = 2;

console.log(mySecondVariable);

let productName = "desktop computer";
console.log(productName);

// Reassigning value
let friend;
friend = "Kate";
friend = "Jane";
friend = "Shawn";
console.log(friend);

// Syntax const variableName variableValue

const pet = "Bruno";
// Reassigning value to const
// pet = "Lala";
console.log(pet);

// const are used for definite value of an object
// const hoursPerDay = 24;
// Local and Global variables

let outerVariable = "hello";  //this is a Global variable

{
    let innerVariable = "Hello World"; //this is a local variable
    console.log(innerVariable);
}

console.log(outerVariable);

const outerExample = "Global Variable";

{
    const innerExample = "Local Variable";
    console.log(innerExample);
}

console.log(outerExample);

// Multiple Declaration

let productCode = "DC017", productBrand = "Dell";

// instead of:
// let productCode = "DC017";
// let productBrand = "Dell";

console.log(productCode, productBrand);

// [SECTION] Data Types

// Strings
// Strings are a series of characters that create a word, phrase, a sentence or anyhting related to creating a text

let country = "Philippines";
let province = "Metro Manila";

// Concatenating Strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in" + ", " + country;
console.log(greeting);

// The escape character "\" in strings in combination with other characters can produce different effects

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early";
message = 'John\'s employees went home early ';
message = 'John\'s sister said, "I can\'t attend.\"';
console.log(message);

// Numbers
let headCount = 26;
console.log(headCount);

// Decimal numbers or fraction
let grade = "98.7";
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining strings and intergers/numbers
console.log("My grade last sem is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Array with different Data Types
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items

// Syntax
            // let/const ObjectName = {
            // propertyA: value,
            // propertyB: value,
            // }

let person = {
    fullname: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contacts: ["09123456789", "09999999999"],
    address: {  //nested object
        houseNumber: "345",
        city: "Manila"
    }
}

console.log(person);

// reassigning value to an array
const anime = ["one piece", "One punch man", "Attack on Titan"];
anime[1] = "Kimetsu no Yaiba";
// Kimetsu no yaiba replaced One Piece because of data structures(arrays start in 0)

console.log(anime);
